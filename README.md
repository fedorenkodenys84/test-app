# Getting Started with this app

Install all dependencies with command npm i

Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation

Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage

Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support

Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap

If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run lint`

Check whole app for lint errors.

### `npm run lint:fix`

Check whole app for lint errors and fix them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment

Show your appreciation to those who have contributed to the project.

## License

## About this app

Technologies used in this application: typescript, saga, react-router, redux toolkit slice, eslint, and prettier.

### How it works

First we retrieve product data from [dummyJSON](https://dummyjson.com/docs/products), by scheduling an action in the ProductList component.

![dispatch products](./src/asstes/imgs/dispatchProducts.png)

ProductSlice is responsible for all product sampling actions, we have 3 actions, Product saga is watching for fetchStart action.

![product saga](./src/asstes/imgs/productSaga.png)

In Product saga we call the getProducts service, which makes axios get requests for products.

![product service](./src/asstes/imgs/getProductsService.png)

And on success we dispatch fetchProductsSuccess, which puts products in state, and on failure we dispatch fetchProductsFailure, which puts an error message in state.

![product slice](./src/asstes/imgs/productSlice.png)

Once we have products in state, we loop through them and display the Product component with the relevant data.

![map through products](./src/asstes/imgs/displayProductList.png)

When we click the Add to Cart button, we dispatch addProduct and add it to the cartSlice state.

On the Cart page, we have the Total Price and Clear All buttons. The total price functionality is handled in the Basket component, where in useEffect, every time we change the price, we reduce all products and accumulate all prices.

![basket](./src/asstes/imgs/basket.png)

When we clear all, we dispatch a clear action, and in the cart slice, we change the product data to an empty array.

![clear](./src/asstes/imgs/clear.png)

When we change the number of items, we dispatch the appropriate action and in the corresponding reducers we check the data.

![cartSlice](./src/asstes/imgs/cartSlice.png)

And each time we change the product, we also save it to the local storage, all functionality is in the component App. Where at mount we retrieve all the products from the local storage and put them into cart slice using the dispatch action setProductsToCart.

![cartSlice](./src/asstes/imgs/cartSlice.png)

## To do

Also this app could be improved by adding notifications on every action, now we display them by alert.
Change markup to divide it on two parts, where we will have products list and cart, and can drag product to cart.
