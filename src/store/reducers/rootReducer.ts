import { combineReducers } from '@reduxjs/toolkit';

import { cartReducers } from 'store/slices/cartSlice';
import { productsReducers } from 'store/slices/productsSlice';

export const rootReducer = combineReducers({ products: productsReducers, cart: cartReducers });
