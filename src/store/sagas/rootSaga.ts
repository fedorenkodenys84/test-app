import { AllEffect, ForkEffect, all, fork } from 'redux-saga/effects';

import { watchFetchProducts } from 'store/sagas/productsSaga';

export function* rootSaga(): Generator<AllEffect<ForkEffect<void>>, void, unknown> {
	yield all([fork(watchFetchProducts)]);
}
