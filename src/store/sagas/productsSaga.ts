import { IProduct } from 'customTypes';
import { call, put, takeLatest } from 'redux-saga/effects';
import { getProducts } from 'services/getProducts';

import { fetchProductsFailure, fetchProductsStart, fetchProductsSuccess } from 'store/slices/productsSlice';

export function* fetchProductsSaga(): Generator<any, void, IProduct[]> {
	try {
		const products: IProduct[] = yield call(getProducts);
		yield put(fetchProductsSuccess(products));
	} catch (error: unknown) {
		if (error instanceof Error) {
			yield put(fetchProductsFailure(error.message));
		}
	}
}

export function* watchFetchProducts(): Generator<any, void, unknown> {
	yield takeLatest(fetchProductsStart().type, fetchProductsSaga);
}
