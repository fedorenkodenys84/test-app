import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { ICartProduct, IProduct } from 'customTypes';

interface CartState {
	products: ICartProduct[];
}

const initialState: CartState = {
	products: [],
};

const cartSlice = createSlice({
	name: 'cart',
	initialState,
	reducers: {
		setProductsToCart: (state, action: PayloadAction<ICartProduct[]>) => {
			state.products = action.payload;
		},
		changeProductCount: (state, action: PayloadAction<{ inputValue: number; product: ICartProduct }>) => {
			const { inputValue, product } = action.payload;
			const productInCart = state.products.find(({ id }) => id === product.id);

			if (!productInCart) {
				return;
			}

			if (inputValue > productInCart.stock) {
				productInCart.inCart = productInCart.stock;
				alert('You cant add more then items in stock');
				return;
			}

			if (inputValue < 1) {
				productInCart.inCart = 1;
				alert('You cant make negative count');
				return;
			}

			productInCart.inCart = inputValue;
		},
		addProduct: (state, action: PayloadAction<IProduct>) => {
			const productInCart = state.products.find(({ id }) => id === action.payload.id);

			if (!productInCart) {
				const newProduct = { ...action.payload, inCart: 1 };
				state.products.push(newProduct);
				return;
			}

			if (productInCart.inCart + 1 > productInCart.stock) {
				alert('You cant add more then items in stock');
				return;
			}

			productInCart.inCart += 1;
		},
		removeProduct: (state, action) => {
			const productInCart = state.products.find(({ id }) => id === action.payload.id);
			if (!productInCart) return;

			if (productInCart.inCart - 1 <= 0) {
				state.products = state.products.filter(({ id }) => action.payload.id !== id);
			}

			productInCart.inCart -= 1;
		},
		clearCart: (state) => {
			state.products = [];
		},
	},
});

export const cartReducers = cartSlice.reducer;
export const { clearCart, addProduct, removeProduct, setProductsToCart, changeProductCount } = cartSlice.actions;
