import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { IProduct } from 'customTypes';

interface IState {
	products: IProduct[];
	loading: boolean;
	error: null | string;
}

const initialState: IState = {
	products: [],
	loading: false,
	error: null,
};

const productsSlice = createSlice({
	name: 'products',
	initialState,
	reducers: {
		fetchProductsStart: (state) => {
			state.loading = true;
			state.error = null;
		},
		fetchProductsSuccess: (state, action: PayloadAction<IProduct[]>) => {
			state.products = action.payload;
			state.loading = false;
			state.error = null;
		},
		fetchProductsFailure: (state, action: PayloadAction<string | null>) => {
			state.loading = false;
			state.error = action.payload;
		},
	},
});

export const productsReducers = productsSlice.reducer;
export const { fetchProductsStart, fetchProductsSuccess, fetchProductsFailure } = productsSlice.actions;
