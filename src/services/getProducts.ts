import axios from 'axios';
import { IProduct } from 'customTypes';

const BASE_URL = 'https://dummyjson.com/products';

interface IGetProducts {
	limit: number;
	products: IProduct[];
	skip: number;
	total: number;
}

export const getProducts = async (): Promise<IProduct[]> => {
	const response = await axios.get<IGetProducts>(BASE_URL);
	const data = response.data.products;
	return data;
};
