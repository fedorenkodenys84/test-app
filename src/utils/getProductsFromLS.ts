import { ICartProduct } from 'customTypes';

export const setProductsToLocalStorage = (): ICartProduct[] | [] => {
	const products = localStorage.getItem('products');

	if (products === null) {
		return [];
	}
	return JSON.parse(products);
};
