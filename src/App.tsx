import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Route, Routes } from 'react-router-dom';

import { setProductsToCart } from 'store/slices/cartSlice';
import { RootState } from 'store/store';

import { Layout } from 'components/Layout';

import { Cart } from 'pages/Cart';
import { ProductList } from 'pages/ProductList';

import { setProductsToLocalStorage } from 'utils/getProductsFromLS';

const App: React.FunctionComponent = () => {
	const dispatch = useDispatch();
	const cartProducts = useSelector((state: RootState) => state.cart.products);

	useEffect(() => {
		const products = setProductsToLocalStorage();

		dispatch(setProductsToCart(products));
	}, []);

	useEffect(() => {
		localStorage.setItem('products', JSON.stringify(cartProducts));
	}, [cartProducts]);

	return (
		<>
			<Routes>
				<Route path="/" element={<Layout />}>
					<Route path="/" element={<ProductList />} />
					<Route path="/cart" element={<Cart />} />
				</Route>
			</Routes>
		</>
	);
};

export default App;
