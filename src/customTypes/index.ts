export enum ProductCategory {
	SMARTPHONES = 'smartphones',
	LAPTOPS = 'laptops',
	FRAGRANCES = 'fragrances',
	SKINCARE = 'skincare',
	GROCERIES = 'groceries',
	'HOME-DECORATION' = 'home-decoration',
}

export interface IProduct {
	id: number;
	title: string;
	description: string;
	price: number;
	discountPercentage: number;
	rating: number;
	stock: number;
	brand: string;
	category: ProductCategory;
	thumbnail: string;
	images: string[];
}

export interface ICartProduct extends IProduct {
	inCart: number;
}
