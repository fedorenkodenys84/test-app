import styled from 'styled-components';

export const CartContainer = styled.div`
	display: flex;
	justify-content: center;
	align-items: start;
`;

export const CartListContainer = styled.div`
	display: 'flex';
	flex-direction: 'column';
`;

export const CartInfoContainer = styled.div`
	position: sticky;
	top: 50%;
	transform: translate(10%, 0);
	text-align: center;
`;

export const CartTitle = styled.h1`
	font-size: 32px;
	font-weight: 500;
	letter-spacing: -0.2px;
	margin-bottom: 10px;
`;

export const CartButton = styled.button`
	width: 250px;
	outline: none;
	text: black;
	background: white;
	border-radius: 9999px;
	border: 2px solid black;
	padding: 10px 0;
	margin-bottom: 20px;
	text-align: text-center;
`;
