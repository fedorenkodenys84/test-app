import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { clearCart } from 'store/slices/cartSlice';
import { RootState } from 'store/store';

import { ProductCart } from 'components/ProductCart';

import { CartButton, CartContainer, CartInfoContainer, CartListContainer, CartTitle } from 'pages/Cart/CartStyled';

export const Cart: React.FunctionComponent = () => {
	const [totalPrice, setTotalPrice] = useState<number>(0);
	const dispatch = useDispatch();

	const { products } = useSelector((state: RootState) => state.cart);
	const productsArr = Object.values(products);

	const clearCartHandler = (): void => {
		dispatch(clearCart());
	};

	useEffect(() => {
		const price = products.reduce((acc, el) => (acc += el.price * el.inCart), 0);
		setTotalPrice(price);
	}, [products]);

	return (
		<CartContainer>
			<CartListContainer>
				{productsArr.map((p) => (
					<ProductCart {...p} key={p.id} />
				))}
			</CartListContainer>
			<CartInfoContainer>
				<CartTitle>Total price: {totalPrice}$</CartTitle>
				<CartButton onClick={clearCartHandler}>Clear all</CartButton>
			</CartInfoContainer>
		</CartContainer>
	);
};
