import styled from 'styled-components';

export const ProductsContainer = styled.div`
	display: flex;
	justify-content: space-around;
	flex-wrap: wrap;
`;
