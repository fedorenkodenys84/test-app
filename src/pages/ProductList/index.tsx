import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { fetchProductsStart } from 'store/slices/productsSlice';
import { RootState } from 'store/store';

import { Product } from 'components/Product';

import { ProductsContainer } from 'pages/ProductList/ProductListStyled';

export const ProductList: React.FunctionComponent = () => {
	const dispatch = useDispatch();

	const { products, loading, error } = useSelector((state: RootState) => state.products);

	useEffect(() => {
		dispatch(fetchProductsStart());
	}, [dispatch]);

	if (loading) {
		return <div>Loading...</div>;
	}

	if (error) {
		return <div>{error}</div>;
	}

	return (
		<ProductsContainer>
			{products.map((p) => {
				return <Product {...p} key={p.id} />;
			})}
		</ProductsContainer>
	);
};
