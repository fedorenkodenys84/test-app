import { FC } from 'react';
import { useDispatch } from 'react-redux';

import { addProduct } from 'store/slices/cartSlice';

import {
	AddCartButton,
	ProductCardImage,
	ProductCardInfo,
	ProductCardText,
	ProductCardTitle,
	ProductCardWrapper,
} from 'components/Product/ProductStyled';

import { IProduct } from 'customTypes/index';

export const Product: FC<IProduct> = (props) => {
	const dispatch = useDispatch();
	const { description, price, rating, stock, title, thumbnail } = props;

	const handleAddItem = (): void => {
		dispatch(addProduct(props));
	};

	return (
		<ProductCardWrapper>
			<ProductCardImage img={thumbnail} />
			<ProductCardInfo>
				<ProductCardTitle>{title}</ProductCardTitle>
				<ProductCardText>{stock} in stock</ProductCardText>
				<ProductCardText>{price}$</ProductCardText>
				<ProductCardText>rating: {rating}</ProductCardText>
			</ProductCardInfo>
			<ProductCardInfo>
				<ProductCardText>{description}</ProductCardText>
			</ProductCardInfo>
			<AddCartButton onClick={handleAddItem}>Add to cart</AddCartButton>
		</ProductCardWrapper>
	);
};
