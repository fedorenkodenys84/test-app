/* eslint-disable @typescript-eslint/explicit-function-return-type */
import styled from 'styled-components';

export const ProductCardWrapper = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	border: 4px solid #ddd;
	margin-bottom: 20px;
	padding: 10px 5px;
	flex: 0 1 31%;
`;

interface IProductCardImage {
	img: string;
}

export const ProductCardImage = styled.div<IProductCardImage>`
	background-color: #dadce2;
	background-position: center;
	background-size: cover;
	border-radius: 5px 5px 0 0;
	width: 100 %;
	height: 350px;
	border: 1px solid black;
	background-image: url(${(props) => props.img});
`;

export const ProductCardInfo = styled.div`
	margin-bottom: 10px;
`;

export const ProductCardTitle = styled.h2`
	font-size: 27px;
	font-weight: 500;
	letter-spacing: -0.2px;
	margin-bottom: 10px;
`;

export const ProductCardText = styled.p`
	font-family: 'Roboto', sans-serif;
	font-weight: 400;
	color: #555;
	line-height: 22px;
`;

export const AddCartButton = styled.button`
	width: 100%;
	outline: none;
	text: black;
	background: white;
	border-radius: 9999px;
	border: 2px solid black;
	padding: 10px 0;
	text-align: text-center;
`;
