import styled from 'styled-components';

export const LayoutContainer = styled.header`
	background: #303338;
	padding: 20px 10px;

	.link {
		color: #ffffff;
		text-decoration: none;
		margin-right: 15px;
	}

	.active {
		text-decoration: underline;
	}
`;
