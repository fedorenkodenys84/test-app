import { NavLink, Outlet } from 'react-router-dom';

import { LayoutContainer } from 'components/Layout/LayoutStyled';

export const Layout: React.FunctionComponent = () => {
	return (
		<>
			<LayoutContainer>
				<NavLink to="/" className="link">
					Products
				</NavLink>
				<NavLink to="/cart" className="link">
					Cart
				</NavLink>
			</LayoutContainer>

			<Outlet />
		</>
	);
};
