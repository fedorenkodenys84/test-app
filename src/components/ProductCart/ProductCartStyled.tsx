/* eslint-disable @typescript-eslint/explicit-function-return-type */
import styled from 'styled-components';

export const ProductCartWrapper = styled.div`
	display: flex;
	align-items: center;
	border: 4px solid #ddd;
	margin-bottom: 20px;
	padding: 10px 5px;
	flex: 0 1 31%;
`;

interface CardImageProps {
	img: string;
}

export const ProductCartImage = styled.div<CardImageProps>`
	background-color: #dadce2;
	background-position: center;
	background-size: cover;
	border-radius: 5px 5px 0 0;
	width: 350px;
	height: 250px;
	margin-right: 20px;
	border: 1px solid black;
	background-image: url(${(props) => props.img});
`;

export const ProductCartContentWrapper = styled.div`
	width: 100%;
	height: 100%;
	display: flex;
	flex-direction: column;
`;

export const ProductCartControlsWrapper = styled.div`
	display: flex;
	flex-direction: row;
	align-items: center;
`;

export const ProductCartInfo = styled.div`
	margin-bottom: 10px;
`;

export const ProductCartTitle = styled.h2`
	font-size: 27px;
	font-weight: 500;
	letter-spacing: -0.2px;
	margin-bottom: 10px;
`;

export const ProductCartText = styled.p`
	font-family: 'Roboto', sans-serif;
	font-weight: 400;
	color: #555;
	line-height: 22px;
`;

export const ProductCartButton = styled.button`
	width: 40px;
	outline: none;
	text: black;
	background: white;
	border-radius: 9999px;
	border: 2px solid black;
	padding: 10px 0;
	margin: 0 10px;
	text-align: text-center;
`;

export const ProductCartInput = styled.input`
	text-align: center;
`;
