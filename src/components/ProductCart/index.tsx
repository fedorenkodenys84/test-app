import { FC } from 'react';
import { useDispatch } from 'react-redux';

import { addProduct, changeProductCount, removeProduct } from 'store/slices/cartSlice';

import {
	ProductCartButton,
	ProductCartContentWrapper,
	ProductCartControlsWrapper,
	ProductCartImage,
	ProductCartInfo,
	ProductCartInput,
	ProductCartText,
	ProductCartTitle,
	ProductCartWrapper,
} from 'components/ProductCart/ProductCartStyled';

import { ICartProduct } from 'customTypes/index';

export const ProductCart: FC<ICartProduct> = (props) => {
	const dispatch = useDispatch();
	const { price, stock, title, thumbnail } = props;

	const handleAddItem = (): void => {
		dispatch(addProduct(props));
	};
	const handleRemoveItem = (): void => {
		dispatch(removeProduct(props));
	};

	const handleInputChange = (e: React.FormEvent<HTMLInputElement>): void => {
		const inputValue = +e.currentTarget.value;

		dispatch(changeProductCount({ inputValue, product: props }));
	};

	return (
		<ProductCartWrapper>
			<ProductCartImage img={thumbnail} />
			<ProductCartContentWrapper>
				<ProductCartInfo>
					<ProductCartTitle>{title}</ProductCartTitle>
					<ProductCartText>{stock} in stock</ProductCartText>
					<ProductCartText>{price}$</ProductCartText>
				</ProductCartInfo>
				<ProductCartControlsWrapper>
					<ProductCartButton onClick={handleRemoveItem}>-</ProductCartButton>
					<ProductCartInput value={props.inCart} type="number" onChange={handleInputChange} />
					<ProductCartButton onClick={handleAddItem}>+</ProductCartButton>
				</ProductCartControlsWrapper>
			</ProductCartContentWrapper>
		</ProductCartWrapper>
	);
};
